<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deposit_action`.
 */
class m171117_110858_create_deposit_action_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('deposit_action', [
            'id'            => $this->primaryKey(),
            'deposit_id'    => $this->integer(11),
            'action'        => "ENUM('commission','accrual')",
            'amount'        => $this->float(),
            'date'          => $this->date()
        ]);

        $this->createIndex('action_deposit_id', 'deposit_action', 'deposit_id');
        $this->addForeignKey('action_deposit_id_to_deposit', 'deposit_action', 'deposit_id', 'deposit', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('deposit_action');
        $this->dropForeignKey('action_deposit_id_to_deposit', 'deposit_action');
    }
}
