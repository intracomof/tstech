<?php

use yii\db\Migration;

/**
 * Handles the creation of table `deposit`.
 */
class m171117_110828_create_deposit_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('deposit', [
            'id'                => $this->primaryKey(),
            'client_id'         => $this->integer(11),
            'amount'            => $this->float(),
            'rate'              => $this->float(),
            'date_of_creation'  => $this->date()
        ]);

        $this->createIndex('deposit_date_of_creation', 'deposit', 'date_of_creation');

        $this->createIndex('deposit_client_id', 'deposit', 'client_id');
        $this->addForeignKey('deposit_to_client', 'deposit', 'client_id', 'client', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('deposit');
        $this->dropForeignKey('deposit_to_client', 'deposit');
    }
}
