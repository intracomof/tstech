<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client`.
 */
class m171117_110806_create_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'first_name'    => $this->string(50),
            'last_name'     => $this->string(50),
            'sex'           => "ENUM('male', 'female')",
            'birthday'      => $this->date()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('client');
    }
}
