<?php
/**
 * Created by PhpStorm.
 * User: alexpin
 * Date: 17.11.17
 * Time: 16:18
 */

namespace app\models;


use yii\db\ActiveRecord;

class DepositAction extends ActiveRecord
{
    const ACTION_COMMISSION    = 'commission';
    const ACTION_ACCRUAL       = 'accrual';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deposit_action';
    }

}