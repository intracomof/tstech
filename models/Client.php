<?php
/**
 * Created by PhpStorm.
 * User: alexpin
 * Date: 17.11.17
 * Time: 16:17
 */

namespace app\models;

use yii\db\ActiveRecord;

class Client extends ActiveRecord
{
    const MALE      = 'male';
    const FEMALE    = 'female';

}