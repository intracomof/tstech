<?php
/**
 * Created by PhpStorm.
 * User: alexpin
 * Date: 17.11.17
 * Time: 16:17
 */

namespace app\models;

use yii\db\ActiveRecord;

class Deposit extends ActiveRecord
{
    const COMMISSION_SMALL  = 0.05;
    const COMMISSION_MEDIUM = 0.06;
    const COMMISSION_LARGE  = 0.07;

    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    public function getDepositActions()
    {
        return $this->hasMany(DepositAction::className(), ['id' => 'deposit_id']);
    }

    /**
     * Рассчет комиссии для депозита
     *
     * @param $thisMonth
     * @return float
     */
    public function calculateCommission($thisMonth) {

        if($this->amount < 1000) {
            $commission = $this->amount*self::COMMISSION_SMALL;
            $commission = $commission > 50 ? 50 : $commission;

        } elseif($this->amount >= 1000 & $this->amount < 10000) {
            $commission = $this->amount*self::COMMISSION_MEDIUM;

        } else {
            $commission = $this->amount*self::COMMISSION_LARGE;
            $commission = $commission > 5000 ? 5000 : $commission;
        }

        $depositCreationMonth = (int)date("m", strtotime($this->date_of_creation));

        // Если депозит был создан в прошлом месяце, то будем взымать толко часть комиссии
        if(($thisMonth - $depositCreationMonth) == 1) {
            $depositCreationDay = (int)date("d", strtotime($this->date_of_creation));
            $commission = $commission/$depositCreationDay;
        }

        $commission = round((float)$commission, 2);
        return $commission;
    }
}