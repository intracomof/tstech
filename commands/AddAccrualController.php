<?php
/**
 * Created by PhpStorm.
 * User: alexpin
 * Date: 17.11.17
 * Time: 16:47
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Deposit;
use app\models\DepositAction;


class AddAccrualController extends Controller
{
    /**
     * Начисление процентов на депозиты
     */
    public function actionIndex()
    {
        $date = date('Y-m-d');
        $thisDay = date('d');
        $lastDayOfThisMonth = date('d', strtotime('last day of this month'));
        $lastDayOfPreviousMonth = date('d', strtotime('last day of previous month'));

        if($thisDay == $lastDayOfThisMonth && $thisDay < $lastDayOfPreviousMonth) {
            $whereCondition = "DAYOFMONTH(date_of_creation) >= '{$thisDay}' AND DAYOFMONTH(date_of_creation) <= '{$lastDayOfPreviousMonth}'";
        } else {
            $whereCondition = "DAYOFMONTH(date_of_creation) = '{$thisDay}'";
        }

        $deposits = Deposit::find()->where($whereCondition)->all();

        /** @var Deposit $deposit */
        foreach($deposits as $deposit) {
            $addAmount = round($deposit->amount*$deposit->rate, 2);

            $deposit->amount = $deposit->amount + $addAmount;
            $deposit->save();

            $depositAction = new DepositAction();
            $depositAction->deposit_id  = $deposit->id;
            $depositAction->action      = DepositAction::ACTION_ACCRUAL;
            $depositAction->amount      = $addAmount;
            $depositAction->date        = $date;
            $depositAction->save();

        }

    }


}
