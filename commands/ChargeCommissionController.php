<?php
/**
 * Created by PhpStorm.
 * User: alexpin
 * Date: 17.11.17
 * Time: 16:57
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Deposit;
use app\models\DepositAction;


class ChargeCommissionController extends Controller
{
    /**
     * Взимание комиссии с депозитов
     */
    public function actionIndex()
    {
        $today = date("Y-m-d");
        $thisMonth = (int)date("m");
        $deposits = Deposit::find()->where("MONTH(date_of_creation) < '{$thisMonth}'")->all();

        /** @var Deposit $deposit */
        foreach($deposits as $deposit) {
            $chargeAmount = $deposit->calculateCommission($today);

            $deposit->amount = $deposit->amount - $chargeAmount;
            $deposit->save();

            $depositAction = new DepositAction();
            $depositAction->deposit_id  = $deposit->id;
            $depositAction->action      = DepositAction::ACTION_COMMISSION;
            $depositAction->amount      = $chargeAmount;
            $depositAction->date        = $today;
            $depositAction->save();

        }

    }


}
